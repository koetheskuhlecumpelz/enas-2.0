import matplotlib.pyplot as plt
import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision.datasets as datasets
import torchvision.transforms as transforms
import torch.distributions.categorical as categorical
import torch.distributions.categorical as one_hot_categorical
import torch.distributions.bernoulli as bernoulli

import child_model as CM
import utils
import time

def sample_operation(P_op, num_samples=1):
    return categorical.Categorical(P_op).sample([num_samples])

def sample_skips(P_skips, num_samples=1):
    #P = torch.rand(size=P_skip.size(), device=utils.device)
    #return P_skips >= P    
    skips = bernoulli.Bernoulli(P_skips).sample([num_samples])
    #return skips.reshape((num_samples, -1))
    return skips.flatten()

def mapped_sampler(P_net, num_samples, map_ops):
    chops = categorical.Categorical(P_net).sample([num_samples])
    for i, v in enumerate(map_ops):
        chops[chops == i] = v
    return chops

def map_to_ops(tens, map_ops):
    for i, v in enumerate(map_ops):
        tens[tens == i] = v
    return tens


def sample_model(Pop, map_ops, Pskip=None):
    if Pskip is None:
        num_nodes = Pop.size(0)
        model_skips = torch.zeros(num_nodes*(num_nodes - 1)//2)
    else:
        model_skips = sample_skips(Pskip)
    model_ops = sample_operation(Pop).squeeze()
    for i, v in enumerate(map_ops):
        model_ops[model_ops == i] = v
    print('\nSampling model:')
    print('ops: ', model_ops.shape)
    print('skips: ', model_skips.shape)
    
    cm = CM.ChildModel(model_ops, model_skips)
    return cm
    

# returns the accuracies of a given list of torch child models on one random test data batch
def test_one_batch(models, test_set):
    accuracies = []
    test_size = len(test_set)
    randbatch_ind = np.random.randint(test_size)
    bdata, btarget = test_set[randbatch_ind]
    for model in models:
        model.eval()
        correct = 0
        with torch.no_grad():
            #t1 = time.time()
            output = model(bdata)
            pred = output.argmax(dim=1, keepdim=True) # get the index of the max log-probability
            correct += pred.eq(btarget.view_as(pred)).sum().item()
            
            accuracy = correct/len(bdata)
            accuracies.append(accuracy)
            #print("testing time = {:.0f}s".format(time.time() - t1))
    return accuracies

# trains the given model for one pass/epoch through the given training data
def train1(model, train_set, optimizer, loss_func=F.nll_loss, log_interval=10, max_batches=None):
    
    batch_size = len(train_set[0][1])
    # only train on the first 'max_samples' images
    if max_batches is None:
        num_batches = len(train_set)
    else:
        # TODO choose batches randomly
        train_set = train_set[0:max_batches]
        num_batches = len(train_set)
    train_size = num_batches*batch_size
	
    model.train()
    t1 = time.time()
    for batch_idx, (data, target) in enumerate(train_set):
        
        optimizer.zero_grad()
        output = model(data)
        
        loss = loss_func(output, target)

        loss.backward()
        optimizer.step()

        if batch_idx % log_interval == 0: 
            print('Train [{}/{} ({:.0f}%)]\tLoss: {:.6f}, Time: {:.3f}'.format(batch_idx*batch_size, train_size,
                100*batch_idx/num_batches, loss.item(), time.time() - t1))
            t1 = time.time()

# initialization routine for the controller weights
def controller_init(m):
    if isinstance(m, nn.LSTMCell):
        for p in m.parameters():
            nn.init.uniform_(p, -0.1, 0.1)

class FCController(nn.Module):
    def __init__(self, num_nodes, use_skip_connections=False, num_child_samples=100, learning_rate=0.001, gamma=0.9, input_amplitude=0.01, allowed_ops=[0, 4], layer_sizes=[100, 500, 10], channels=9):
        super(FCController, self).__init__()
        
        #initialize hyper parameters
        self.num_nodes = num_nodes
        self.allowed_ops = allowed_ops
        self.layer_sizes = layer_sizes
        self.num_child_samples = num_child_samples
        self.input_amplitude = input_amplitude
        self.use_skips = use_skip_connections
        self.channels = channels
        
        # define layers
        layers = []
        for lid in range(len(self.layer_sizes)):
            if lid == len(self.layer_sizes) - 1:
                outsize = len(self.allowed_ops)*self.num_nodes
                layers.append(nn.Linear(self.layer_sizes[lid], outsize))
            else:    
                layers.append(nn.Linear(self.layer_sizes[lid], self.layer_sizes[lid + 1]))
                layers.append(nn.ReLU())
        self.fc = nn.Sequential(*layers)
        
        if self.use_skips:
            skip_layers = []
            n_out = (self.num_nodes-1) * self.num_nodes // 2
            for lid,layer_size in enumerate(layer_sizes):
                next_size = layer_sizes[lid+1] if lid<len(layer_sizes)-1 else n_out
                skip_layers.append( nn.Linear(layer_size, next_size) )
                if lid<len(layer_sizes)-1:
                    skip_layers.append( nn.ReLU() )
            self.fc_skips = nn.Sequential(*skip_layers)
        
        # hyperparameters
        self.gamma = gamma # exponential baseline decay
        self.learning_rate = learning_rate

        self.timestep = 1
        
        self.apply(controller_init)
        self.to(utils.device)
 
    def backward(self, childmodel, Pop, Pskips=None):
        for node_ind in range(Pop.size(0)):
            op = childmodel.ops[node_ind].item()
            opid = np.where(np.array(self.allowed_ops) == op)[0][0]
            prob = Pop[node_ind, opid]
            
            ## without GPU
            #weight = 1/prob.clone().detach()
            #prob.backward(weight, retain_graph=True)

            prob.backward(1/prob, retain_graph=True)
            #prob.backward(torch.Tensor([1/prob.item()], device=utils.device), retain_graph=True)
            
            if self.use_skips:
                start_ind = (node_ind-1)*node_ind // 2
                prob = Pskips[ start_ind:start_ind+node_ind ]
                for prev_node_ind in range(node_ind):
                    p_skip = prob[prev_node_ind]
                    p_skip.backward(1/p_skip, retain_graph=True)
                    #p_skip.backward(torch.Tensor([1/p_skip.item()], device=utils.device), retain_graph=True)
        
    def update_step_naive(self, R, baseline=True):
        num_models = len(R)
        b = 0 # baseline to reduce variance
        
        with torch.no_grad():
            for pi, (name, p) in enumerate(self.named_parameters()):
                pbefore = p.clone()
                for n in range(num_models):
                    r = R[n]
                    if baseline:
                        b = (1 - self.gamma)*r + self.gamma*b # update baseline
                    p += self.learning_rate*(r - b)*p.grad/num_models
                print("mean relative update for {} = {}".format(name, ((pbefore - p)/p).mean()))

        self.timestep += 1
    
    def forward(self):
        inp = nn.init.uniform_(torch.zeros(1, self.layer_sizes[0], device=utils.device), -self.input_amplitude, self.input_amplitude)
            
        out = self.fc(inp).view(self.num_nodes, -1)
        P_ops = F.softmax(out, dim=1)
            
        if self.use_skips:
            skip_inp = nn.init.uniform_(torch.zeros(1, self.layer_sizes[0], device=utils.device), -self.input_amplitude, self.input_amplitude)
            skip_out = self.fc_skips(skip_inp)
            P_skips = torch.sigmoid(skip_out)
            print('forward, reurning two variables')
            return P_ops, P_skips
        
        else: 
            print('forward, reurning one variables')
            return P_ops#, cm
    
    def step1(self, epoch, train_set, shared_parameters, optimizer=optim.SGD, opt_args={"lr": 0.01, "momentum": 0.8, "weight_decay": 1e-4, "nesterov": True},
              train_args={"loss_func": F.cross_entropy, "log_interval": 10, "max_batches": None}):
        ts = time.time()
        print("Starting step 1 of epoch {}".format(epoch))
        
        if self.use_skips:
            P_ops, P_skips = self.forward()
        else:
            P_ops = self.forward()
            P_skips = None
        cm = sample_model(P_ops, self.allowed_ops, P_skips)
        
        tcm = cm.to_torch_model(shared_parameters, channels=self.channels)
        opt = optimizer(tcm.parameters(), **opt_args)
        train1(tcm, train_set, opt, **train_args)
        print("End of step 1, took {:.0f}s".format(time.time() - ts))
        return tcm.get_shared_weights(shared_parameters)
    
    def step2(self, epoch, test_set, shared_parameters):
        print("Starting step 2 of epoch {}".format(epoch))
        ts = time.time()
        print("Generating {} child models ...".format(self.num_child_samples))

        tcms = []        
        for _ in range(self.num_child_samples):
            self.zero_grad()

            if self.use_skips:
                Pop, P_skips = self.forward()
            else:
                Pop = self.forward()
                P_skips = None
        
            cm = sample_model(Pop, self.allowed_ops, P_skips)
            tcm = cm.to_torch_model(shared_parameters, channels=self.channels)
            tcms.append(tcm)
            self.backward(tcm.childmodel, Pop)
        
        print("Validating child models ...")
        acc = test_one_batch(tcms, test_set) # test child model performance
        
        print("Mean validation accuracy = {:.1f}%".format(np.mean(acc)*100))
        print("Updating controller weights ...")
        self.update_step_naive(acc) # update controller weights naively
        print("End of step 2 of epoch {} took {:.0f} seconds to complete".format(epoch, time.time() - ts))