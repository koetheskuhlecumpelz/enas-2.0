import torch
import torch.nn as nn
import numpy as np
import time, os, glob
P = os.path.sep



#convert two 1D-tensors of lower left matrix indices to flattened triangular index
def tri2flat( ii,jj ):

    assert ii.size[0] == jj.size[0] and torch.all(ii > jj) \
        and torch.all(ii>=0) and torch.all(jj>=0), 'indices invalid for triangle matrix'
    
    return ii * (ii-1) / 2 + jj

#convert flattened triangular idx to two 1D-tensors of matrix indeces
def flat2tri(idxs):
    ii = torch.zeros(len(idxs))
    jj = torch.zeros(len(idxs))
    for ind, idx in enumerate(idxs):
        i = 1
        while (idx >= i * (i-1)/2):
            i += 1
        i -= 1
        j = idx - (i-1)*i/2
        ii[ind] = i
        jj[ind] = j
    return ii, jj

#find appropriate device
if torch.cuda.is_available():
    device = 'cuda:0'
else:
    device = 'cpu:0'    
device = torch.device(device)

class DeviceDataLoader():
    def __init__(self, dl):
        self.dl = dl
   
    def __iter__(self):
        for x,y in self.dl:
            yield x.to(device),y.to(device)
            
    def __len__(self):
        return len(self.dl)
   
#    def __iter__(self):
#        return iter(self.dl)
    
    @property
    def dataset(self):
        return self.dl.dataset
    
    @property
    def batch_size(self):
        return self.dl.batch_size
   
def get_device(obj):
    if isinstance(obj, nn.Module):
        devices = dict()
        
        ## Paramter List
        #for param in obj.parameters():
        #    devices.append(param.device)
        
        #State dict        
        state = obj.state_dict()
        for param in state:
            devices[param] = state[param].device
            
        devices = '\n\t'.join([d + ": "+ str(devices[d]) for d in devices])
        return "devices:\n\t" + devices
    
    elif isinstance(obj, torch.Tensor):
        return obj.device
    else:
        print('object of type',type(obj),'has no device')



# save current state of the ENAS search including the most promising child model so far
def save_checkpoint(experiment_name, enas_epoch, controller, best_child, shared_weights, best_accs, avg_accs, path="..\\saved"):
    full_path = path + P + experiment_name
    if not os.path.exists(full_path):
        os.makedirs(full_path)
    
    g = time.gmtime()
    check_name  = experiment_name+"_"+str(g.tm_year)+"_"+str(g.tm_mon)+"_"+str(g.tm_mday)+"_"+str(g.tm_hour)+"_"+str(g.tm_min)+"_"+str(g.tm_sec)+".pt"
    torch.save({"epoch": enas_epoch,
                "controller": controller.state_dict(),
                "best_child_model": best_child.childmodel,
                "best_child": best_child.state_dict(),
                "shared_weights": shared_weights,
                "best_accs": best_accs,
                "avg_accs": avg_accs
                },
                full_path+P+check_name)

# load previous state of ENAS
def load_checkpoint(experiment_name, file_name=None, path=".."+P+"saved", map_location=device):
    exp_path = path + P + experiment_name
    if file_name is None:
        list_of_files = glob.glob(exp_path + P + "*")
        file_name = max(list_of_files, key=os.path.getctime).split(P)[-1]

    checkpoint = torch.load(exp_path+P+file_name, map_location)
    return checkpoint


