import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision.datasets as datasets
import torchvision.transforms as transforms
import torch.distributions.categorical as categorical
import torch.distributions.categorical as one_hot_categorical
import torch.distributions.bernoulli as bernoulli


import child_model as CM
import utils
import time
import copy

# returns the accuracies of a given list of torch child models on one random test data batch
def test_one_batch(models, test_set):
    accuracies = []
    test_size = len(test_set)
    randbatch_ind = np.random.randint(test_size)
    bdata, btarget = test_set[randbatch_ind]
    for model in models:
        model.eval()
        correct = 0
        with torch.no_grad():
            #t1 = time.time()
            output = model(bdata)
            pred = output.argmax(dim=1, keepdim=True) # get the index of the max log-probability
            correct += pred.eq(btarget.view_as(pred)).sum().item()
            
            accuracy = correct/len(bdata)
            accuracies.append(accuracy)
            #print("testing time = {:.0f}s".format(time.time() - t1))
    return accuracies

# trains the given model for one pass/epoch through the given training data
def train1(model, train_set, optimizer, loss_func=F.nll_loss, log_interval=10, max_batches=None):
    
    batch_size = len(train_set[0][1])
    # only train on the first 'max_samples' images
    if max_batches is None:
        num_batches = len(train_set)
    else:
        # TODO choose batches randomly
        train_set = train_set[0:max_batches]
        num_batches = len(train_set)
    train_size = num_batches*batch_size
	
    model.train()
    t1 = time.time()
    for batch_idx, (data, target) in enumerate(train_set):
        
        optimizer.zero_grad()
        output = model(data)
        
        loss = loss_func(output, target)

        loss.backward()
        optimizer.step()

        if batch_idx % log_interval == 0: 
            print('Train [{}/{} ({:.0f}%)]\tLoss: {:.6f}, Time: {:.3f}'.format(batch_idx*batch_size, train_size,
                100*batch_idx/num_batches, loss.item(), time.time() - t1))
            t1 = time.time()

class MCTSController:
    def __init__(self, nodes, num_child_samples, test_set, C=1, mcts_iter=100, allowed_ops=[0, 1, 2, 3, 4, 5], channels=CM.CHANNELS):
        self.num_nodes = nodes
        self.num_child_samples = num_child_samples
        self.allowed_ops = allowed_ops
        self.C = C
        self.channels = channels
        self.mcts_iterations = mcts_iter
        self.rules = OpSampling(self.num_nodes, self.allowed_ops, test_set)
    
    # runs MCTS from every node every time sampling the most promising action/operation
    def forward(self, omega):
        ops = []
        next_root = None
        for n in range(self.num_nodes):
            t1 = time.time()
            print("sample node {}".format(n))
            mcts = VanillaMCTS(ops, self.rules, C=self.C, root=next_root, shared_weights=omega, channels=self.channels)
            best_op = mcts.run_iter(self.mcts_iterations)
            next_root = mcts.root.children[best_op]
            ops.append(best_op)
            print("sampled op {} in {}s".format(best_op, time.time() - t1))
        return ops
            
class OpSampling:
    def __init__(self, num_nodes, allowed_ops, test_set):
        self.num_nodes = num_nodes
        self.allowed_ops = allowed_ops
        self.test_set = test_set
        
    def num_players(self):
        return 1

    def get_current_player_id(self, state):
        return 0

    def is_valid_state(self, state):
        return len(state) <= self.num_nodes

    def is_terminal(self, state):
        return len(state) == self.num_nodes

    def get_initial_state():
        return []

    def get_actions(self, state):
        return self.allowed_ops

    def perform_action(self, action, state):
        if not self.is_terminal(state):
            state.append(action)
            return state
    
    def reward(self, state, omega, channels):
        sk = torch.zeros(self.num_nodes*(self.num_nodes - 1)//2)
        cm = CM.ChildModel(torch.Tensor(state), sk)
        r = test_one_batch([cm.to_torch_model(omega, channels)], self.test_set)
        return np.array([r[0]])
        
class Node:
    def __init__(self, state, parent=None, statistics={}):
        self.state = state
        self.parent = parent
        self.children = {}
        self.statistics = statistics
        
    def expand(self, action, next_state):
        child = Node(next_state, parent=self)
        self.children[action] = child
        return child
    
class VanillaMCTS:
    def __init__(self, state, gamerules, C=1, root=None, shared_weights=None, channels=CM.CHANNELS):
        self.game = gamerules
        self.C = C
        self.shared_weights = None
        self.channels = channels
        if root is None:
            self.root = Node(state)
        else:
            self.root = root
        self.root.statistics = {"visits": 0, "reward": np.zeros(self.game.num_players())}
        
    def rollout(self, node):
        roll_state = copy.deepcopy(node.state)
        while not self.game.is_terminal(roll_state):
            act_set = self.game.get_actions(roll_state)
            action = act_set[np.random.randint(0, len(act_set) - 1)]
            roll_state = self.game.perform_action(action, roll_state)
        return self.game.reward(roll_state, self.shared_weights, self.channels)
    
    def is_fully_expanded(self, node):
        return len(self.game.get_actions(node.state)) == len(list(node.children))
    
    def best_action(self, node):
        children = list(node.children.values())
        visits = np.array(([child.statistics["visits"] for child in children]))
        rewards = np.array(([child.statistics["reward"] for child in children]))
        total_rollouts = node.statistics["visits"]
        pid = self.game.get_current_player_id(node.state)
        ucb = rewards[:,pid]/visits + self.C*np.sqrt(2*np.log(total_rollouts)/visits)
        
        ucb = np.nan_to_num(ucb)
        
        best_ucb_ind = np.random.choice(np.flatnonzero(ucb == ucb.max()))
        return list(node.children.keys())[best_ucb_ind]
    
    def tree_policy(self, node):
        while not self.game.is_terminal(node.state):
            if not self.is_fully_expanded(node):
                act_set = np.setdiff1d(self.game.get_actions(node.state), list(node.children.keys()))
                action = act_set[np.random.randint(0, len(act_set))]
                newstate = copy.deepcopy(node.state)
                newstate = self.game.perform_action(action, newstate)
                childnode = node.expand(action, newstate)
                childnode.statistics = {"visits": 0,  "reward": np.zeros(self.game.num_players())}
                return childnode
            else:
                node = node.children[self.best_action(node)]
        return node

    def backup(self, node, reward):
        while not node is None:
            node.statistics["visits"] += 1
            node.statistics["reward"] += reward
            node = node.parent
    
    def run_iter(self, iterations):
        for i in range(iterations):
            selected_node = self.tree_policy(self.root)
            reward = self.rollout(selected_node)
            self.backup(selected_node, reward)
        return self.best_action(self.root)