
import torch
import torch.nn.functional as F
import torch.optim as optim
import matplotlib.pyplot as plt

import utils
import data as D
import child_model as CM
import child_visualizer as V
import time

def test_child(model, test_loader, output_interval=10):
    
    model.eval()
    correct = 0
    
    size_dataset = len(test_loader.dataset)
    print(size_dataset)
    
    t1 = time.time()
    
    print('Testing child model.')
    with torch.no_grad():
        for data, target in test_loader:
        	output = model(data)
        	pred = output.argmax(dim=1, keepdim=True) # get the index of the max log-probability
        	correct += pred.eq(target.view_as(pred)).sum().item()
        accuracy = correct/size_dataset
    
    print("testing time = {:.0f}s".format(time.time() - t1))
    return accuracy

def train_child(model, train_set, optimizer, loss_func, epochs=5, log_interval=100):
    batch_size = train_set.batch_size
    num_batches = len(train_set)
    train_size = num_batches*batch_size
	
    model.train()
    t1 = time.time()
    
    for epoch in range(epochs):
        print('Epoch ', epoch, ' / ', epochs )
        
        for batch_idx, (data, target) in enumerate(train_set):
            
            optimizer.zero_grad()
            output = model(data)
            
            loss = loss_func(output, target)
    
            loss.backward()
            optimizer.step()
    
            if batch_idx % log_interval == 0: 
                print('Train [{}/{} ({:.0f}%)]\tLoss: {:.6f}, Time: {:.3f}'.format(batch_idx*batch_size, train_size,
                    100*batch_idx/num_batches, loss.item(), time.time() - t1))
        t1 = time.time()



def pick_best_from_checkpoint(experiment_name, weight_init="old", use_droput=False):
    
    
    #load checkpoint
    checkpoint = utils.load_checkpoint(experiment_name, file_name=None)
    child = checkpoint['best_child_model']
    state = checkpoint['best_child']
    omega = checkpoint['shared_weights']
    accuracy = max( checkpoint['best_accs'] )
    
    #show child model
    V.draw_child(child)
    print('Best acc:', accuracy)
    
    
    #Initialization strategies
    if weight_init == 'new':
        child = child.to_torch_model()
        
    if weight_init == 'current':
        child = child.to_torch_model(omega)
        
    elif weight_init == 'old':
        child = child.to_torch_model(omega)
        child.load_state_dict(state)
        
    elif weight_init == 'nudge':
        child = child.to_torch_model()
        weights = child.state_dict()
        for key in state:
            state[key] = state[key] + weights[key]
        child.load_state_dict(state)
    else:
        assert False, 'invalid initialization strategy ' + str(weight_init)
    return child
            
    
