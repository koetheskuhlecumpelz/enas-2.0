import matplotlib.pyplot as plt
import numpy as np
import itertools

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision.datasets as datasets
import torchvision.transforms as transforms
import torch.distributions.categorical as categorical
import torch.distributions.categorical as one_hot_categorical
import torch.distributions.bernoulli as bernoulli

import child_model as CM
import utils
import time

def sample_operation(P_op, num_samples=1):
    return categorical.Categorical(P_op).sample([num_samples])

# returns the accuracies of a given list of torch child models on one random test data batch
def test_one_batch(models, test_set):
    accuracies = []
    test_size = len(test_set)
    randbatch_ind = np.random.randint(test_size)
    bdata, btarget = test_set[randbatch_ind]
    for model in models:
        model.eval()
        correct = 0
        with torch.no_grad():
            #t1 = time.time()
            output = model(bdata)
            pred = output.argmax(dim=1, keepdim=True) # get the index of the max log-probability
            correct += pred.eq(btarget.view_as(pred)).sum().item()
            
            accuracy = correct/len(bdata)
            accuracies.append(accuracy)
            #print("testing time = {:.0f}s".format(time.time() - t1))
    return accuracies

# trains the given model for one pass/epoch through the given training data
def train1(model, train_set, optimizer, loss_func=F.nll_loss, log_interval=10, max_batches=None):
    
    batch_size = len(train_set[0][1])
    # only train on the first 'max_samples' images
    if max_batches is None:
        num_batches = len(train_set)
    else:
        # TODO choose batches randomly
        train_set = train_set[0:max_batches]
        num_batches = len(train_set)
    train_size = num_batches*batch_size
	
    model.train()
    t1 = time.time()
    for batch_idx, (data, target) in enumerate(train_set):
        
        optimizer.zero_grad()
        output = model(data)
        
        loss = loss_func(output, target)

        loss.backward()
        optimizer.step()

        if batch_idx % log_interval == 0: 
            print('Train [{}/{} ({:.0f}%)]\tLoss: {:.6f}, Time: {:.3f}'.format(batch_idx*batch_size, train_size,
                100*batch_idx/num_batches, loss.item(), time.time() - t1))
            t1 = time.time()

# initialization routine for the controller weights
def controller_init(m):
    if isinstance(m, nn.LSTMCell):
        for p in m.parameters():
            nn.init.uniform_(p, -0.1, 0.1)

class OpController(nn.Module):
    def __init__(self, num_nodes, num_child_samples=100, num_hidden=100, learning_rate=0.001, gamma=0.9):
        super(OpController, self).__init__()
        
        #initialize hyper parameters
        self.num_nodes = num_nodes
        self.num_hidden = num_hidden
        self.num_ops = len(CM.OPERATION_NAMES)
        self.num_child_samples = num_child_samples
        
        # define LSTM cell and output layers
        self.op_cell = nn.LSTMCell(self.num_hidden, self.num_hidden)
        self.op_out = nn.Linear(self.num_hidden, self.num_ops)
        
        # hyperparameters
        self.gamma = gamma # exponential baseline decay
        self.learning_rate = learning_rate

        self.timestep = 1
        
        self.apply(controller_init)
        self.to(utils.device)
 
    def backward(self, childmodel, Pop):
        for node_ind in range(Pop.size(0)):
            op = childmodel.ops[node_ind].int()
            prob = Pop[node_ind, op]
            prob.backward(torch.Tensor([1/prob.item()]), retain_graph=True)
        
    def update_step_naive(self, R, baseline=True):
        num_models = len(R)
        b = 0 # baseline to reduce variance
        
        with torch.no_grad():
            for pi, (name, p) in enumerate(self.named_parameters()):
                pbefore = p.clone()
                for n in range(num_models):
                    r = R[n]
                    if baseline:
                        b = r + self.gamma*b # update baseline
                    p += self.learning_rate*(r - b)*p.grad/num_models
                print("mean relative update for {} = {}".format(name, ((pbefore - p)/p).mean()))

        self.timestep += 1
    
    def forward(self):
        g_emb = nn.init.uniform_(torch.zeros(1, self.num_hidden, device=utils.device), -1e-6, 1e-6)
        batch_size = g_emb.size(0)
        #h_prev = torch.zeros(self.num_nodes + 1, batch_size, self.num_hidden, device=utils.device)
        #c_prev = torch.zeros(self.num_nodes + 1, batch_size, self.num_hidden, device=utils.device)
        #inp_prev = torch.zeros(self.num_nodes + 1, batch_size, self.num_hidden, device=utils.device)
        h_prev = []
        h_prev.append(torch.zeros(batch_size, self.num_hidden, device=utils.device))
        c_prev = []
        c_prev.append(torch.zeros(batch_size, self.num_hidden, device=utils.device))
        inp_prev = []
        inp_prev.append(g_emb)
        P_skips = torch.zeros((int((self.num_nodes - 1)*self.num_nodes/2)), device=utils.device)
        P_ops = torch.zeros((self.num_nodes, self.num_ops), device=utils.device)
        
        model_ops = torch.zeros(self.num_nodes)
        model_skips = torch.zeros(self.num_nodes*(self.num_nodes - 1)//2)
        
        for i in range(self.num_nodes): # iterate over cells
            # operation cell
            h_out, c_out = self.op_cell(inp_prev[-1], (h_prev[-1], c_prev[-1]))

            # calculate prob distribution operation at this node
            op_out_out = self.op_out(h_out).squeeze(0)

            P_op = F.softmax(op_out_out, dim=0)

            P_ops[i] = P_op
            
            # sample operation
            opid = sample_operation(P_op)
            model_ops[i] = opid
            
            # define next input
            next_inp_prev = torch.zeros(g_emb.size(), device=utils.device)
            next_inp_prev[0][opid] = 1

            # store hidden and cell state and input
            #inp_prev[i + 1] = next_inp_prev
            #h_prev[i + 1] = h_out
            #c_prev[i + 1] = c_out
            inp_prev.append(next_inp_prev)
            h_prev.append(h_out)
            c_prev.append(c_out)
        
        cm = CM.ChildModel(model_ops, model_skips)
        return P_ops, P_skips, cm
    
    def step1(self, epoch, train_set, shared_parameters, optimizer=optim.SGD, opt_args={"lr": 0.01, "momentum": 0.8, "weight_decay": 1e-4, "nesterov": True},
              train_args={"loss_func": F.cross_entropy, "log_interval": 10, "max_batches": 100}):
        ts = time.time()
        print("Starting step 1 of epoch {}".format(epoch))
        Pop, Psk, cm = self.forward()
        tcm = cm.to_torch_model(shared_parameters)
        opt = optimizer(tcm.parameters(), **opt_args)
        train1(tcm, train_set, opt, **train_args)
        print("End of step 1, took {:.0f}s".format(time.time() - ts))
        return tcm.get_shared_weights(shared_parameters)
    
    def step2(self, epoch, test_set, shared_parameters):
        print("Starting step 2 of epoch {}".format(epoch))
        ts = time.time()
        print("Generating {} child models ...".format(self.num_child_samples))
        tcms = []
        for _ in range(self.num_child_samples):
            self.zero_grad()
            Pop, Psk, cm = self.forward()
            tcm = cm.to_torch_model(shared_parameters)
            tcms.append(tcm)
            self.backward(tcm.childmodel, Pop)
        
        print("Validating child models ...")
        acc = test_one_batch(tcms, test_set) # test child model performance
        
        print("Mean validation accuracy = {:.1f}%".format(np.mean(acc)*100))
        print("Updating controller weights ...")
        self.update_step_naive(acc) # update controller weights naively
        print("End of step 2 of epoch {} took {:.0f} seconds to complete".format(epoch, time.time() - ts))